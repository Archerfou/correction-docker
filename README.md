# Travaux Pratiques : Docker

## Création des conteneurs sans docker-compose

On crée un réseau en mode bridge afin que les containers puissent communiquer entre eux via le nom d'hôte :

```
$ docker network create -d bridge api
```

Puis on démarre nos conteneurs en les mettant sur ce réseau :

```
$ docker run --network api --rm -d --name api-backend -p 8001:8001 api-backend
$ docker run --network api --rm -d --name api-frontend -p 8000:8000 api-frontend
```

Pour vérifier le bon fonctionnement, on effectue une requête HTTP sur chacun des containers. Pour retrouver l'adresse IP du container, on utilise `docker inspect` :

```
$ docker inspect api-frontend
[... Truncated ]
"IPv6Gateway": "",
            "MacAddress": "",
            "Networks": {
                "api": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": [
                        "0c879272e5c6"
                    ],
                    "NetworkID": "993ba68b32862631355bac04f1b1ba214a5a2865d5f7861cb1d5c5f3e56421a8",
                    "EndpointID": "a6953204a6efe238e1a94fd0f64fd71e99cef9fb240e3e141727879ec971a238",
                    "Gateway": "172.19.0.1",
                    "IPAddress": "172.19.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:13:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
```

Puis avec curl :

```
$ curl http://172.19.0.2:8000
{
  "body": "Je suis une r\u00e9ponse du backend"
}
```

## Objectifs

L'objectif de ce TP est de créer des conteneurs et d'automatiser la phase de construction et de déploiement des conteneurs dans un contexte Ops. Pour réaliser ce TP, il vous faudra respecter scrupuleusement les conventions de nommage et de configuration demandées, afin que le bot de correction soit en mesure de vérifier que tout fonctionne comme attendu. Cela passe également par le respect des formats de données utilisés. Il vous sera demandé de construire deux conteneurs communiquant entre eux. Ces deux conteneurs présentent des APIs simple permettant de renvoyer des données au format JSON via le protocole HTTP. Ces deux applicatifs sont fournis et doivent uniquement être déployés, il ne s'agit donc pas de développement mais de mise en production, orienté système et services.

## Les applicatifs

Les applicatifs sont publiés sur un dépôt Github, dans les sous dossiers de ce dépôts :

- API Frontend : api-frontend
- API Backend : api-backend

L'API Frontend est un script écrit en python et utilisant le framework Flask. Il présente 3 endpoints qui sont :

 - / (GET) : Cette uri renvoi est censé renvoyer une réponse HTTP au format JSON avec un code 200. Un call est fait sur l'API Backend

 - /healthcheck (GET) : Cette uri renvoi un code HTTP 200. Elle doit être utilisée afin de vérifier le bon fonctionnement de l'applicatif par Docker

 - /frontend-traceback (GET) : Cette uri permet de générer une erreur mettant fin au processus en cours d'éxécution dans le container. Cela permet de vérifier, lors de la correction, que l'ID du processus est bien le 1 et que l'applicatif se recharge  automatiquement en cas d'arrêt inopiné.

- /backend-traceback (GET) : Cette uri permet de générer une erreur mettant fin au processus en cours d'éxécution dans le container. Cela permet de vérifier, lors de la correction, que l'ID du processus est bien le 1 et que l'applicatif se recharge  automatiquement en cas d'arrêt inopiné.

 L'API Backend est sensiblement similaire à l'API frontend. Elle présente deux endpoints qui sont :

 - / (GET) : Cette uri renvoi est censé renvoyé une réponse avec un code HTTP 200.
 - /healthcheck (GET) : Cette uri renvoi un code HTTP 200. Elle doit être utilisée afin de vérifier le bon fonctionnement de l'applicatif par Docker

 - /traceback (GET) : Cette uri permet de générer une erreur mettant fin au processus en cours d'éxécution dans le container. Cela permet de vérifier, lors de la correction, que l'ID du processus est bien le 1 et que l'applicatif se recharge  automatiquement en cas d'arrêt inopiné.

Chaque applicatif propose un fichier README.md indiquant les pré-requis de fonctionnement du programme (langage utilisé, version de langage et bibliothèques nécessaires).

## Rendu

Vous devrez faire un fork du dépôt Github afin de récupérer les deux applicatifs et de pouvoir envoyer vos modifications sur votre dépôt.
Vous devrez mettre à disposition deux `Dockerfile` (un par applicatif) ainsi qu'un fichier `docker-compose.yaml` pour le déploiement automatique des deux applicatifs. Vous devriez avoir une arborescence telle que :

```
.
├── LICENSE
├── README.md
├── api-backend
│   ├── Dockerfile
│   └── api.py
│   └── Readme.md
├── api-frontend
│   ├── Dockerfile
│   └── api.py
│   └── Readme.md
└── docker-compose.yaml
```

Vous devrez également publier vos images sur un compte Dockerhub que vous aurez préalablement crée. La version fonctionnelle doit être taggué en `latest`. L'adresse des images sur le Dockerhub devra figurer dans un champs `LABEL` du docker file, ayant pour valeur `hub_url`.

Voici comment devrait se lancer les deux applicatifs après avoir rempli votre fichier `docker-compose.yaml` :

[![asciicast](https://asciinema.org/a/mzbhERZTHnBuf6lEZJ9iMNGEH.svg)](https://asciinema.org/a/mzbhERZTHnBuf6lEZJ9iMNGEH)
